# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from model_utils.models import TimeStampedModel
from django.db import models
from query_sets import *


class Company(models.Model):
    company_name = models.CharField(max_length=20)
    city_choices = (
        ('חולון', 'חולון'),
        ('הרצליה', 'הרצליה'),
        ('יהוד', 'יהוד')
    )
    city = models.CharField(max_length=20,
                            choices=city_choices,
                            default='שומכלום')
    phone_num = models.CharField(max_length=20)
    is_active = models.BooleanField(default=False)
    objects = CompanyQuerySet().as_manager()

    def natural_key(self):
        return self.company_name

    def __unicode__(self):
        return self.company_name

    def __str__(self):
        return self.company_name


class Car(TimeStampedModel, models.Model):
    car_name = models.CharField(max_length=20)
    car_price = models.FloatField(max_length=200)
    companies = models.ManyToManyField(Company, through='PriceList')
    objects = CarQuerySet().as_manager()

    def natural_key(self):
        return self.car_name

    def __unicode__(self):
        return self.car_name

    def __str__(self):
        return self.car_name


class PriceList(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    price_at_company = models.FloatField(max_length=200)
    objects = PriceListQuerySet().as_manager()

    def __unicode__(self):
        return self.car.car_name + " - " + self.company.company_name

    def __str__(self):
        return self.car.car_name + " - " + self.company.company_name