from django.conf.urls import url,patterns,include
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter
from leasing.viewsets.car_viewset import CarViewSet
from leasing.viewsets.company_viewset import CompanyViewSet
from leasing.viewsets.pricelist_viewset import PriceListViewSet
from rest_framework import routers
from django.views.generic import TemplateView

from . import views

admin.autodiscover()
router = ExtendedDefaultRouter()
(
    router.register('cars', CarViewSet, base_name='cars'),
    router.register('companies', CompanyViewSet, base_name='companies'),
    router.register('price_lists', CompanyViewSet, base_name='price_lists')
)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^cars_from_active_companies/$', views.cars_from_active_companies, name='cars_from_active_companies'),
    url(r'^price_list_for_car/(?P<car_name>.+)/$', views.price_list_for_car, name='price_list_for_car'),
    url(r'^price_list_for_company/(?P<company_name>.+)$', views.price_list_for_company, name='price_list_for_company'),
    url(r'^companies_that_active_and_their_name_contains_the_hebrew_letter_yud/',
        views.companies_that_active_and_their_name_contains_the_hebrew_letter_yud,
        name='companies_that_active_and_their_name_contains_the_hebrew_letter_yud'),
    url(r'^min_price_for_each_company/', views.min_price_for_each_company, name='min_price_for_each_company'),
    url('', include(router.urls))    #as in the esg presentation
]
