# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.test import Client
from leasing.models import Car, Company, PriceList
import json


class TestQueries(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_company = Company(company_name="שיפודי סמי", city="חולון", phone_num="053-2748619", is_active="True")
        self.test_company.save()
        self.test_company2 = Company(company_name="test_company", city="חולון", phone_num="053-2749619", is_active="True")
        self.test_company2.save()
        self.test_car = Car(car_name="פרארי רושה", car_price=8000)
        self.test_car.save()
        self.test_car2 = Car(car_name="test_car", car_price=500)
        self.test_car2.save()
        self.p = PriceList(price_at_company=8000, car=self.test_car, company=self.test_company)
        self.p.save()
        self.p2 = PriceList(price_at_company=300, car=self.test_car2, company=self.test_company2)
        self.p2.save()

    def test_cars_from_active_companies_status_code(self):
        response = self.client.get('/leasing/cars_from_active_companies/')
        self.assertEqual(response.status_code, 200)

    def test_is_cars_from_active_companies_contains_answer(self):
        response = self.client.get('/leasing/cars_from_active_companies/')
        print response.content
        self.assertContains(response, "test_car")

    def test_price_list_for_car_status_code(self):
        response = self.client.get('/leasing/price_list_for_car/פרארי רושה/')
        self.assertEqual(response.status_code, 200)

    def test_is_price_list_for_car_status_code_contains_answer(self):
        response = self.client.get('/leasing/price_list_for_car/פרארי רושה/')
        self.assertContains(response, 8000)

    def test_companies_that_active_and_their_name_contains_the_hebrew_letter_yud_status_code(self):
        response = self.client.get('/leasing/companies_that_active_and_their_name_contains_the_hebrew_letter_yud/')
        self.assertEqual(response.status_code, 200)

    def test_is_companies_that_active_and_their_name_contains_the_hebrew_letter_yud_status_code_contains_answer(self):
        response = self.client.get('/leasing/companies_that_active_and_their_name_contains_the_hebrew_letter_yud/')
        self.assertContains(response, "053-2748619")

    def test_price_list_for_company_status_code(self):
        response = self.client.get('/leasing/price_list_for_company/שיפודי סמי')
        self.assertEqual(response.status_code, 200)

    def test_is_price_list_for_company_status_code_status_code(self):
        response = self.client.get('/leasing/price_list_for_company/שיפודי סמי')
        self.assertContains(response, 8000)
