# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.test import Client
from leasing.models import Car, Company, PriceList
import pandas as pd
from pandas.util.testing import assert_frame_equal


class TestQueries(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_company = Company(company_name="שיפודי סמי", city="חולון", phone_num="053-2748619", is_active="True")
        self.test_car = Car(car_name="פרארי רושה", car_price=8000)
        self.test_company.save()
        self.test_car.save()
        self.p = PriceList(price_at_company=8000, car=self.test_car, company=self.test_company)
        self.p.save()

    def test_cars_from_active_companies_status_code(self):
        num_of_query_rows = len(Car.objects.values('car_name', 'car_price').filter(companies__is_active=True).distinct())
        self.assertEqual(num_of_query_rows, 1)

    def test_price_list_for_car(self):
        num_of_query_rows = len(PriceList.objects.values('car', 'company', 'price_at_company').filter(car__car_name="פרארי רושה"))
        self.assertEqual(num_of_query_rows, 1)

    def test_companies_that_active_and_their_name_contains_the_hebrew_letter_yud(self):
        num_of_query_rows = len(Company.objects.values(
            'company_name', 'city', 'phone_num', 'is_active'
            ).filter(is_active=True, company_name__contains="י"))
        self.assertEqual(num_of_query_rows, 1)

    def test_price_list_for_company(self):
        num_of_query_rows = len(PriceList.objects.values('car', 'company', 'price_at_company').filter(
        company__company_name="שיפודי סמי"))
        self.assertEqual(num_of_query_rows, 1)

