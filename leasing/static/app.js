/**
 * Created by DEV on 17/05/2016.
 */

var main = function() {
    $('#submit_cars_from_active_companies_query').click(function(e) {
        e.preventDefault();
            $.ajax({
                url: '/leasing/cars_from_active_companies/',
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function (response) {
                    console.log(response);
                    var parsed_json_query = jQuery.parseJSON( response );
                    var html_table = "<table>" +
                            "<tr>" +
                            "<th>שם רכב</th>" +
                            "<th>מחיר</th>" +
                            "</tr>";
                    for (var i=0; i<parsed_json_query.length; i++) {
                        html_table = html_table +
                            "<tr>" +
                            "<td>" + parsed_json_query[i].fields.car_name + "</td>" +
                            "<td>" + parsed_json_query[i].fields.car_price + "</td>" +
                            "</tr>";
                    }
                    html_table = html_table + "</table>";
                    $('#cars_from_active_companies_form').remove();
                    $('#cars_from_active_companies_div').append(html_table);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });

    $('#price_list_for_car_query').click(function(e) {
        e.preventDefault();
            $.ajax({
                url: '/leasing/price_list_for_car/' + $('#price_list_for_car_input').val(),
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function(response) {
                    console.log(response);
                    var car_name = $('#price_list_for_car_input').val();
                    var parsed_json_query = jQuery.parseJSON( response );
                    var html_table = "<table>" +
                            "<tr>" +
                            "<th>שם רכב</th>" +
                            "<th>שם חברה</th>" +
                            "<th>מחיר</th>" +
                            "</tr>";
                    for (var i=0; i<parsed_json_query.length; i++) {
                        html_table = html_table +
                                "<tr>" +
                                "<td>" + car_name + "</td>" +
                                "<td>" + parsed_json_query[i].fields.company + "</td>" +
                                "<td>" + parsed_json_query[i].fields.price_at_company + "</td>" +
                                "</tr>";
                    }
                    html_table = html_table + "</table>";
                    $('#price_list_for_car_form').remove();
                    $('#price_list_for_car_div').append(html_table);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });

    $('#price_list_for_company_query').click(function(e) {
        e.preventDefault();
        var company_name = $('#price_list_for_company_input').val();
            $.ajax({
                url: '/leasing/price_list_for_company/' + company_name,
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function (response) {
                    console.log(response);
                    var parsed_json_query = jQuery.parseJSON( response );
                    var html_table = "<table>" +
                            "<tr>" +
                            "<th>שם חברה</th>" +
                            "<th>שם רכב</th>" +
                            "<th>מחיר</th>" +
                            "<th>שם רכב</th>" +
                            "</tr>";
                    for (var i=0; i<parsed_json_query.length; i++) {
                        html_table = html_table +
                                "<tr>" +
                                "<td>" + company_name + "</td>" +
                                "<td>" + parsed_json_query[i].fields.car + "</td>" +
                                "<td>" + parsed_json_query[i].fields.price_at_company + "</td>" +
                                "</tr>";
                    }
                    html_table = html_table + "</table>";
                    $('#price_list_for_company_form').remove();
                    $('#price_list_for_company_div').append(html_table);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });

    $('#companies_that_active_and_their_name_contains_the_hebrew_letter_yud_query').click(function(e) {
        e.preventDefault();
            $.ajax({
                url: '/leasing/companies_that_active_and_their_name_contains_the_hebrew_letter_yud/',
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function (response) {
                    console.log(response);
                    var parsed_json_query = jQuery.parseJSON( response );
                    var check = "";
                    var html_table = "<table>" +
                            "<tr>" +
                            "<th>שם חברה</th>" +
                            "<th>עיר</th>" +
                            "<th>מספר טלפון</th>" +
                            "<th>האם פעילה</th>" +
                            "</tr>";
                    for (var i=0; i<parsed_json_query.length; i++) {
                        check = (parsed_json_query[i].fields.is_active)?("כן") : ("לא");
                        html_table = html_table +
                                "<tr>" +
                                "<td>" + parsed_json_query[i].fields.company_name + "</td>" +
                                "<td>" + parsed_json_query[i].fields.city + "</td>" +
                                "<td>" + parsed_json_query[i].fields.phone_num + "</td>" +
                                "<td>" + check + "</td>" +
                                "</tr>";
                    }
                    html_table = html_table + "</table>";
                    $('#companies_that_active_and_their_name_contains_the_hebrew_letter_yud_form').remove();
                    $('#companies_that_active_and_their_name_contains_the_hebrew_letter_yud_div').append(html_table);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });

    $('#min_price_for_each_company_query').click(function(e) {
        e.preventDefault();
            $.ajax({
                url: '/leasing/min_price_for_each_company/',
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                async: false,
                success: function (response) {
                    console.log(response);
                    var parsed_json_query = jQuery.parseJSON( response );
                    var html_table = "<table>" +
                            "<tr>" +
                            "<th>שם חברה</th>" +
                            "<th>שם רכב</th>" +
                            "<th>מחיר</th>" +
                            "</tr>";
                    for (var i=0; i<parsed_json_query.length; i++) {
                        html_table = html_table +
                                "<tr>" +
                                "<td>" + parsed_json_query[i].fields.company + "</td>" +
                                "<td>" + parsed_json_query[i].fields.car + "</td>" +
                                "<td>" + parsed_json_query[i].fields.price_at_company + "</td>" +
                                "</tr>";
                    }
                    html_table = html_table + "</table>";
                    $('#min_price_for_each_company_form').remove();
                    $('#min_price_for_each_company_div').append(html_table);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    });

};

$(document).ready(main);