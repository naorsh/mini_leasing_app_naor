# -*- coding: utf-8 -*-
from django.shortcuts import render

from django.http import HttpResponse, JsonResponse, FileResponse
from .models import Car, Company, PriceList
from django_pandas.io import read_frame
from StyleFrame import StyleFrame
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import serializers


def index(request):
    return render_to_response("index.html", {}, context_instance=RequestContext(request))


def cars_from_active_companies(request):
    query_result = Car.objects.cars_from_active_companies()
    # response = save_and_serve_query_result(query_result)
    # query_result = serializers.serialize("json", query_result)
    json_query = serializers.serialize("json", query_result)
    response = HttpResponse(json_query)
    return response


def price_list_for_car(request, car_name):
    query_result = PriceList.objects.price_list_for_car(car_name)
    # response = save_and_serve_query_result(query_result)
    json_query = serializers.serialize("json", query_result, use_natural_foreign_keys=True)
    response = HttpResponse(json_query)
    return response


def price_list_for_company(request, company_name):
    # query_result = PriceList.objects.values('car', 'company', 'price_at_company').filter(
    #     company__company_name=company_name)
    query_result = PriceList.objects.price_list_for_company(company_name)
    # response = save_and_serve_query_result(query_result)
    json_query = serializers.serialize("json", query_result, use_natural_foreign_keys=True)

    response = HttpResponse(json_query)
    return response


def companies_that_active_and_their_name_contains_the_hebrew_letter_yud(request):
    # query_result = Company.objects.values(
    #     'company_name', 'city', 'phone_num', 'is_active'
    #      ).filter(is_active=True, company_name__contains="י")
    query_result = Company.objects.companies_that_active_and_their_name_contains_the_hebrew_letter_yud()
    # response = save_and_serve_query_result(query_result)
    json_query = serializers.serialize("json", query_result)
    response = HttpResponse(json_query)
    return response


def min_price_for_each_company(request):
    query_result = PriceList.objects.min_price_for_each_company()
    # response = save_and_serve_query_result(query_result)
    json_query = serializers.serialize("json", query_result, use_natural_foreign_keys=True)
    response = HttpResponse(json_query)
    return response


def save_and_serve_query_result(query_result):
    df = read_frame(query_result)
    sf = StyleFrame(df)
    sf.set_row_height(1, 40)
    ew = StyleFrame.ExcelWriter(path='query_result.xlsx')
    sf.to_excel(ew, row_to_add_filters=0, columns_and_rows_to_freeze='A2')
    ew.save()
    file_to_serve = open('query_result.xlsx', 'rb')
    response = HttpResponse(file_to_serve.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename= file.xlsx"
    file_to_serve.close()
    return response
