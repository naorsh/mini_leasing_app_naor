# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Min, F


class CarQuerySet(models.QuerySet):
    def cars_from_active_companies(self):
        # return self.values('car_name', 'car_price').filter(companies__is_active=True).distinct()
        return self.filter(companies__is_active=True).distinct()


class CompanyQuerySet(models.QuerySet):
    def companies_that_active_and_their_name_contains_the_hebrew_letter_yud(self):
        # return self.values(
        # 'company_name', 'city', 'phone_num', 'is_active'
        #  ).filter(is_active=True, company_name__contains="י")
        return self.filter(is_active=True, company_name__contains="י")


class PriceListQuerySet(models.QuerySet):
    def price_list_for_car(self, car_name):
        # return self.values('car', 'company', 'price_at_company').filter(car__car_name=car_name)
        # return self.filter(car__car_name=car_name)
        return self.filter(car__car_name=car_name)

    def price_list_for_company(self, company_name):
        # return self.values('car', 'company', 'price_at_company').filter(company__company_name=company_name)
        return self.filter(company__company_name=company_name)

    def min_price_for_each_company(self):
        return self.annotate(min_price=Min('company__pricelist__price_at_company')).filter(price_at_company=F('min_price')).only('company', 'car', 'price_at_company')

