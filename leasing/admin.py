from django.contrib import admin
# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Car, Company, PriceList

admin.site.register(Car)
admin.site.register(Company)
admin.site.register(PriceList)
